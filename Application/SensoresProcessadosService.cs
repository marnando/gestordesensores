﻿//-----------------------------------------------------------------------
// <copyright file="SensoresProcessadosService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace GestorEventos.Application
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Data.Config;
    using Data.Entidades;
    using Data.Interfaces;

    /// <summary>
    /// Sensores Processados Service.
    /// </summary>
    public class SensoresProcessadosService : ISensoresProcessadosService
    {
        /// <summary>
        /// Indicador de Timestamp baseado em Epoch Converter.
        /// </summary>
        private static readonly DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        /// <summary>
        /// Contexto do Bando de Dados.
        /// </summary>
        private readonly Contexto repository;

        /// <summary>
        /// Interface Sensor Service.
        /// </summary>
        private readonly ISensorService sensorService;

        /// <summary>
        /// Initializes a new instance of the <see cref="SensoresProcessadosService"/> class.
        /// </summary>
        /// <param name="repositorio">Respositório da aplicação.</param>
        /// <param name="sensorService">Interface Sensor Service.</param>
        public SensoresProcessadosService(Contexto repositorio, ISensorService sensorService)
        {
            this.repository = repositorio;
            this.sensorService = sensorService;
        }

        /// <summary>
        /// COnvert de Epoch para DateTime.
        /// </summary>
        /// <param name="unixTime">Unix Timestamp.</param>
        /// <returns>DateTime formatado.</returns>
        public DateTime FromUnixTime(long unixTime)
        {
            return Epoch.AddSeconds(unixTime);
        }

        /// <summary>
        /// Conversão de DateTime para Unix timestamp.
        /// </summary>
        /// <param name="data">Data para conversão.</param>
        /// <returns>Unix Timestamp.</returns>
        public long FromDateTime(DateTime data)
        {
            DateTimeOffset dateOffSet = DateTime.SpecifyKind(data, DateTimeKind.Utc);
            return dateOffSet.ToUnixTimeSeconds();
        }

        /// <summary>
        /// Listar todos os sensores processados.
        /// </summary>
        /// <returns>Listar sensores processados.</returns>
        public IEnumerable<SensoresProcessados> Listar()
        {
            return this.repository.SensoresProcessados.Select(x => x).ToList();
        }

        /// <summary>
        /// Capturar Sensor.
        /// </summary>
        /// <param name="aSensor">Sensor detectado.</param>
        /// <returns>Excessões da função.</returns>
        private async Task CapturaSensor(Sensor aSensor)
        {
            try
            {
                await Task.Run(() =>
                {
                    if (this.sensorService.AdicionarEvento(aSensor) == true)
                    {
                        this.repository.SensoresProcessados.Add(new SensoresProcessados()
                        {
                            Sequencial = 0,
                            Sensor = aSensor,
                            Timestamp = FromDateTime(DateTime.Now),
                        });
                    }
                });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
