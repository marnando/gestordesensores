﻿//-----------------------------------------------------------------------
// <copyright file="SensorService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace GestorEventos.Application
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Data.Config;
    using Data.Entidades;
    using Data.Interfaces;

    /// <summary>
    /// Sensor Service.
    /// </summary>
    public class SensorService : ISensorService
    {
        /// <summary>
        /// Contexto do Bando de Dados.
        /// </summary>
        private readonly Contexto repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="SensorService"/> class.
        /// </summary>
        /// <param name="repositorio">Respoítorio da aplicação.</param>
        public SensorService(Contexto repositorio)
        {
            this.repository = repositorio;
        }

        /// <summary>
        /// Listar os sensores armazenados.
        /// </summary>
        /// <returns>Lista de sensores.</returns>
        public IEnumerable<Sensor> Listar()
        {
            return this.repository.Sensor.Select(x => x).ToList();
        }

        /// <summary>
        /// Validação da configuraçãos eventos transmitidos.
        /// </summary>
        /// <param name="evento">Informação dos eventos.</param>
        /// <returns>True para quando tem erros na validação e False para quando não existir erro.</returns>
        public bool ValidarSensor(Sensor evento)
        {
            return !string.IsNullOrEmpty(evento.Valor);
        }

        /// <summary>
        /// Adicionar Evento.
        /// </summary>
        /// <param name="evento">Captura do evento.</param>
        /// <returns>adiciona o evento no banco.</returns>
        public bool AdicionarEvento(Sensor evento)
        {
            try
            {
                this.repository.Sensor.Add(evento);
                this.repository.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return false;
        }
    }
}
