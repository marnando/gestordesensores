# GestorDeSensores

<ABBR> Solução proposta deve assumir o envio de sinais enviados aleatoriamente por sensores, que assincronamente deverão ser capturados e transformados em dados analíticos. Todas as informações deverão estar registradas em bando de dados relacional.

Esta aplicação está orientada a serviços.


Tecnologias relacionadas:

Aspnet MVC Framework Core (2.1.1), Enitiy Framework (2.1.1), StyleCop, Entity Framework Sql Server (2.1.1), Linq.

Melhorias Futuras:

Specflow para aplicação dos testes na camada de serviço.
Finalizar o processamento dos sensores na aplicação.

</ABBR>