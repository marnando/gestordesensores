﻿//-----------------------------------------------------------------------
// <copyright file="HomeController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace GestorEventos.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using Data.Interfaces;
    using System;
    using GestorEventos.Utils;

    /// <summary>
    /// Home Controller.
    /// </summary>
    public class HomeController : Controller
    {
        /// <summary>
        /// Interface Sensor Service.
        /// </summary>
        private readonly ISensorService sensorService;

        /// <summary>
        /// Interface Sensores Processados Service.
        /// </summary>
        private readonly ISensoresProcessadosService sensoresProcessadosService;

        /// <summary>
        /// Initializes a new instance of the <see cref="HomeController"/> class.
        /// Classe Controller.
        /// </summary>
        /// <param name="sensorService">Servide Sensor.</param>
        /// <param name="sensoresProcessadosService">Sensores processados.</param>
        public HomeController(ISensorService sensorService, ISensoresProcessadosService sensoresProcessadosService)
        {
            this.sensorService = sensorService;
            this.sensoresProcessadosService = sensoresProcessadosService;
        }

        /// <summary>
        /// View Index.
        /// </summary>
        /// <returns>View index.</returns>
        public IActionResult Index()
        {
            return this.View();
        }

        /// <summary>
        /// Listar Sensores Processados.
        /// </summary>
        /// <returns>Lista de sensores processados.</returns>
        public JsonResult ListarSensoresProcessados()
        {
            try
            {
                return this.Json(new { success = true, model = this.sensoresProcessadosService.Listar() });
            }
            catch (Exception ex)
            {
                return this.Json(new { success = false, error = Constantes.NaoFoiPossivelRecuperarAListaDeSensores });
            }
        }
    }
}
