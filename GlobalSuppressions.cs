﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1208:System using directives should be placed before other using directives", Justification = "<Pendente>", Scope = "namespace", Target = "~N:GestorEventos.Controllers")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1210:Using directives should be ordered alphabetically by namespace", Justification = "<Pendente>", Scope = "namespace", Target = "~N:GestorEventos.Controllers")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1201:Elements should appear in the correct order", Justification = "<Pendente>", Scope = "member", Target = "~F:GestorEventos.Controllers.HomeController.contexto")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1642:Constructor summary documentation should begin with standard text", Justification = "<Pendente>", Scope = "member", Target = "~M:GestorEventos.Controllers.HomeController.#ctor(Data.Config.Contexto)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1629:Documentation text should end with a period", Justification = "<Pendente>", Scope = "member", Target = "~M:GestorEventos.Application.SensorService.#ctor(Data.ISensorService)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1642:Constructor summary documentation should begin with standard text", Justification = "<Pendente>", Scope = "member", Target = "~M:GestorEventos.Application.SensorService.#ctor(Data.ISensorService)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1642:Constructor summary documentation should begin with standard text", Justification = "<Pendente>", Scope = "member", Target = "~M:GestorEventos.Controllers.HomeController.#ctor(Data.Config.Contexto,Data.ISensorService)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1642:Constructor summary documentation should begin with standard text", Justification = "<Pendente>", Scope = "member", Target = "~M:GestorEventos.Controllers.HomeController.#ctor(Data.Config.Contexto,ISensorService)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Qualidade do Código", "IDE0052:Remover membros particulares não lidos", Justification = "<Pendente>", Scope = "member", Target = "~F:GestorEventos.Controllers.HomeController.sensoresProcessadosService")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1101:Prefix local calls with this", Justification = "<Pendente>", Scope = "member", Target = "~M:GestorEventos.Application.SensoresProcessadosService.CapturaSensor(Data.Entidades.Sensor)~System.Threading.Tasks.Task")]