﻿//-----------------------------------------------------------------------
// <copyright file="Constantes.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace GestorEventos.Utils
{
    /// <summary>
    /// Constantes da aplicação.
    /// </summary>
    public class Constantes
    {
        /// <summary>
        /// Mensagem informativa: Não foi possível recuparar a lista de sensores processados.
        /// </summary>
        public const string NaoFoiPossivelRecuperarAListaDeSensores = "Não foi possível recuparar a lista de sensores processados.";
    }
}
